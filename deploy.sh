#!/usr/bin/env sh

set -e

npm run docs:build
cd docs/.vuepress/dist

git init
git add -A
git commit -m 'deploy'

git push -f ssh://git@gitlab.com/vue-prototypes/vue3-component-library.git main:pages

cd -
