import peerDepsExternal from "rollup-plugin-peer-deps-external";
import vue from "rollup-plugin-vue";

export default [
  {
    input: "src/index.js",
    output: [
      {
        format: "esm",
        file: "dist/library.mjs",
      },
      {
        format: "cjs",
        file: "dist/library.js",
      },
    ],
    // Adding plugins here tells roll-up to "externalize" those dependencies, not including them in the final build. This is a component library
    plugins: [vue(), peerDepsExternal()],
  },
];
